var elixir = require('laravel-elixir');

/*
styles-search is from s4. It's more up to date but needs to be
implemented in less.

web-styles.less is from s3.
 */

elixir(function(mix) {
    mix.less('app.less', 'public/css/app.css')
        .less('web-styles.less', 'public/css/web-styles.css')
        .copy('resources/assets/css/styles-search.css', 'public/css/')
        .copy('resources/assets/css/styles-providers.css', 'public/css/')
        .copy('resources/assets/css/vendor/', 'public/css/')
        .copy('resources/assets/js/vendor/', 'public/js/')
        .copy('resources/assets/js/globio.js', 'public/js/')
        .copy('resources/assets/js/landing-search.js', 'public/js/')
        .copy('resources/assets/js/landing-providers.js', 'public/js/')
        .copy('resources/assets/js/landing-payments.js', 'public/js/')
        .copy('resources/assets/js/organization-edit.js', 'public/js/')
        .copy('resources/assets/js/signup.js', 'public/js/')
       .copy('resources/assets/js/refer.js', 'public/js/')
        .copy('resources/assets/js/upload.js', 'public/js/')
        .version([
            'js/landing-search.js',
            'js/landing-providers.js',
            'js/landing-payments.js',
            'js/organization-edit.js',
            'js/signup.js',
            'js/refer.js',
            'js/upload.js',
//            'js/campaign-analytics.js'
        ]);
});

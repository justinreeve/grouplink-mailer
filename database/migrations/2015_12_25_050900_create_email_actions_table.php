<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateEmailActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_actions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('campaign_id')->index();
            $table->text('email_id')->index();
            $table->text('action_type');
            $table->text('ipaddr');
            $table->text('url')->nullable();
            $table->jsonb('location')->nullable();
            $table->text('ua_raw');
            $table->jsonb('ua_parsed')->nullable();
            $table->timestamp('ts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('email_actions');
    }
}

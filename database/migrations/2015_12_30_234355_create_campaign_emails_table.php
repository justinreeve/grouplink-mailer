<?php

use App\Models\EmailStatus;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateCampaignEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_emails', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('campaign_id');
            $table->text('hash');
            $table->text('mandrill_id')->nullable();
            $table->text('email');
            $table->text('first_name');
            $table->text('last_name');
            $table->text('template_slug')->nullable();
            $table->timestamp('send_at');
            $table->text('status')->default(EmailStatus::NOT_SUBMITTED);
			$table->timestamp('submitted_at')->nullable();
            $table->timestamp('sent_at')->nullable();
            $table->timestamp('deferred_at')->nullable();
            $table->timestamp('opened_at')->nullable();
            $table->timestamp('clicked_at')->nullable();
            $table->timestamp('hard_bounce_at')->nullable();
            $table->timestamp('soft_bounce_at')->nullable();
            $table->timestamp('unsubscribe_at')->nullable();
            $table->timestamp('marked_spam_at')->nullable();
            $table->timestamp('rejected_at')->nullable();
            $table->text('bounce_reason')->nullable();
            $table->text('bounce_detail')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campaign_emails');
    }
}

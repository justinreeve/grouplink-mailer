;(function($){

    if (!String.prototype.trim) {
        (function(){
            // Make sure we trim BOM and NBSP
            var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
            String.prototype.trim = function () {
                return this.replace(rtrim, "");
            }
        })();
    }

    //var apiUrlSearch = 'http://'+S3SERVER+'/search'; // devel
    var apiUrlSearch = 'http://localhost:8083/search'; // devel

    var searchResults;
    var noSubmit = false;

    /**
     * Mapbox Instance
     * @type {Object}
     */
    var map;

    var tourLayer;

    /**
     * Overlaying Map Spiderfier Instance
     * @type {Object}
     */
    var spiderfier;

    /**
     * Array of Google Map markers from a user search
     * @type {Array}
     */
    var searchMarkers   = [];

    /**
     * Array of Tour markers that share the same primary
     * place lattitude and longitude.
     *
     * @type {Array}
     */
    var sameLocationMarkers = [];
    var spiderfying = false;

    var markerClusterer = {};

    var tourDetailsPanel;

    /**
     * Template partials
     *
     * - Search result
     * - Search result details panel
     */

    var templateResult,
        templateResultDetail;

    templateCompany =
        '<hr/>'
        +   '<div class="company">'
        +   '<div class="pull-left">'
        +   '<h3><%= name %></h3>'
        +   '<i class="fa fa-globe"></i><%= url %><br>'
        +   '<i class="fa fa-phone"></i><%= phone %><br>'
        +   '<i class="fa fa-envelope"></i><%= email %>'
        +   '</div>'
        +   '<div class="pull-right"><%= logo %></div>'
        + '</div>';

    templateCarousel =
        '<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">'
        +   '<div class="carousel-inner" role="listbox">'
        +     '<%= images %>'
        +   '</div>'
        +   '<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">'
        +     '<i class="fa fa-chevron-left glyphicon glyphicon-chevron-left"></i>'
        +     '<span class="sr-only">Previous</span>'
        +   '</a>'
        +   '<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">'
        +     '<i class="fa fa-chevron-right glyphicon glyphicon-chevron-right"></i>'
        +     '<span class="sr-only">Next</span>'
        +     '</a>'
        + '</div>';

    templateResult =
        '<div class="media<%=view%>" data-place="<%= sku %>" data-id="<%= id %>">'
        +     '<div class="clearfix">'
        +       '<h2 class="media-heading text-overflow"><%= title %></h2>'
        +       '<strong class="pull-right">From <%= price %> <%= currency %></strong>'
        +     '</div>'
        +   '<a class="media-left thumbnail" href="#">'
        +     '<%=thumbnailList%>'
        +     '<%=thumbnailGrid%>'
        +   '</a>'
        +   '<div class="media-body">'
        +     '<p class="description"><%= description %></p>'
        +     '<p><span class="tag"><i class="fa fa-clock-o"></i><%= duration %></span> <%= guestTypes %></p>'
        +   '</div>'
        +   '<%= detail %>'
        + '</div>';

    templateResultDetail =
        '<div class="tour-details" data-place="<%= sku %>" data-id="<%= id %>">'
        +   '<div class="tour-images"><%= tourImages %></div>'
        +   '<div class="title clearfix">'
        +     '<h2 class="pull-left"><%= title %> <%= guestTypes %></h2>'
        +     '<a href="#" class="pull-right btn btn-red">Book Now</a>'
        +   '</div>'
        +   '<div class="tour-content">'
        +     '<div id="overview" class="collapse in">'
        +     '<div class="clearfix">'
        +       '<span class="price pull-left">From <%= price %> <%= currency %> </span>'
        +       '<span class="duration pull-right"><%= duration %></span>'
        +     '</div>'
        +     '<div class="description"><%= description %></div>'
        +     '<%= places %>'
        +     '<%= activities %>'
        +     '<%= companyDetails %>'
        +     '</div>' //end #overview
        +   '</div>' //end .tour-content
        + '</div>';

    // +     '<button class="btn btn-red">Book</button>'

    /**
     * Get allowed guest type icons for tour
     *
     * @param  {[type]} result [description]
     * @return {[type]}        [description]
     */
    function getAllowedGuestTypes(result, showLabel) {

        var html = '';

        if ( result.allowChildren ) {
            showLabel = showLabel ? 'Child Friendly' : '';
            html += '<span class="icon-child tag"><i class="fa fa-child" alt="Child Friendly Tour"></i>'+showLabel+'</span>';
        }

        if ( result.allowWheelchair ) {
            showLabel = showLabel ? 'Wheelchair Friendly' : '';
            html += '<span class="icon-wheelchair tag"><i class="fa fa-wheelchair" alt="Wheelchair Friendly Tour"></i>'+showLabel+'</span>';
        }

        return html;
    }

    /**
     * Get tour duration in human readable time.
     *
     * @param  {[type]} result [description]
     * @return {[type]}        [description]
     */
    function getTourDuration(result) {

        var duration = result.duration * 60;

        var units = {
            "day"   : 24*60*60,
            "hour"  : 60*60,
            "minute": 60,
            "second": 1
        }

        var result = [];

        for(var unit in units) {

            var time =  Math.floor(duration/units[unit]);

            if (time == 1) {
                result.push(time + " " + unit);
            }

            if (time >= 2) {
                result.push(time + " " + unit + "s");
            }

            duration %= units[unit]
        }
        // console.log(result.join(' '));
        return result.join(' ');

    }

    /**
     * Get tour description.
     *
     *
     * @param  {[type]} result [description]
     * @param  {[type]} type   [description]
     * @return {[type]}        [description]
     */
    function getTourDescription(result, type) {

        if ( type === 'full' ) {

            result = result.description
                .replace(/\s(id|class|style).*?(?=>|$)/g, '')     //strip disallowed attributes
                .replace(/(\\{1,})(\"|\')/g, '$2')                //strip backslashes, preserve single/double quotes
                .replace(/(\r\n{1,}|\r{2,}|\n{2,})/g, '</p><p>'); //replace new lines with paragraph tags

            //wrap result in paragraph tags to properly close out first and last paragraphs
            return '<p>' + result + '</p>';

        }

        var description = result.description;

        if ( _.has(result, 'metaDescription') ) {
            description = result.metaDescription;
        }

        if ( type === 'meta' ) {
            //todo, get rid of jQuery.text()...
            description = $('<div>' + description + '</div>').text();
        }

        if ( description && description.length > 106 ) {
            return description.substr(0, 106).trim() + '...';
        }

        return description;

    }

    function getSkuById(id) {
        return $('#search-results .media[data-id="'+id+'"]').attr('data-place');
    }

    function getCurrentResultId() {
        return parseInt(tourDetailsPanel.attr('data-id'));
    }

    function getNextResultId() {

        var count     = getResults().length;
        var currentId = getCurrentResultId();

        if ( (currentId + 1) > count ) {

            return 1;

        } else {

            return parseInt(getCurrentResultId() + 1);

        }

    }

    function getPrevResultId() {

        var count     = getResults().length;
        var currentId = getCurrentResultId();

        if ( (currentId - 1) < 1 ) {

            return count;

        } else {

            return parseInt(getCurrentResultId() - 1);

        }

    }

    function getNextResult() {
        return getSkuById( getNextResultId() );
    }

    function getPrevResult() {
        return getSkuById( getPrevResultId() );
    }

    function showTourDetailsBySku(sku) {

        var result;

        if ( spiderfying ) {
            return false;
        }

        $('#search-controls').show();

        result           = $('#search-results .media[data-place="'+sku+'"]');
        tourDetailsPanel = result.find('.tour-details').detach();

        $('#search-results').prepend(tourDetailsPanel.fadeIn("fast"));

        $('#search-results .media').hide();

        $('#search-results').addClass('grid-view');

        $('#page-results span').text( result.attr('data-id') + ' of ' + getResults().length);

        $('#sidebar').scrollTop(0);
    }

    /**
     * Hide visible tour details panel.
     * @return {mixed}
     */
    function hideTourDetails() {

        if ( _.isUndefined(tourDetailsPanel) ) {
            return false;
        }

        $('#search-controls').hide();

        var sku = tourDetailsPanel.attr('data-place');
        // console.log('hiding', sku);
        $('#search-results .media[data-place="'+sku+'"]').append(tourDetailsPanel.toggle().detach());

        $('#search-results .media').not('.tour-details').show();

        $('#search-results').removeClass('grid-view');

    }

    function setResults(results) {
        //console.log('setResults, results : %o',results);
        return searchResults = _.pluck(results.hits.hits,'_source');
    }

    function getResults() {
        return searchResults;
    }

    function showNoResultMessage() {
        $('#no-result').find('.term').text($('#sidebar input[name="query"]').val());
        $('#no-result').fadeIn();
    }

    function hideNoResultMessage() {
        $('#no-result').fadeOut();
    }

    function showSearchResults() {

        var html = [];

        if ( ! getResults().length ) {
            showNoResultMessage();
            return;
        }

        _.each(getResults(), function(result, key){

            var detailPartial,
                resultPartial;

            //build the result detail panel
            company = _.template(
                templateCompany,
                {
                    logo   : getCompanyLogo(result),
                    url    : getCompanyUrl(result),
                    name   : result.companyName,
                    phone  : result.companyPhone,
                    email  : getCompanyEmail(result),
                    address: result.companyAddress ? result.companyAddress : ''

                }
            );

            carousel = _.template(
                templateCarousel,
                {
                    images     : getCarouselImages(result),

                }
            );

            //build the result detail panel
            detailPartial = _.template(
                templateResultDetail,
                {
                    id            : (key + 1),
                    title         : result.title,
                    price         : result.price,
                    currency      : result.currency,
                    sku           : result.sku,
                    tourImages    : hasImages(result) ? carousel : '',
                    companyDetails: company,
                    description   : getTourDescription(result, 'full'),
                    duration      : getTourDuration(result),
                    guestTypes    : getAllowedGuestTypes(result),
                    places        : getPlaceTags(result),
                    activities    : getActivityTags(result)

                }
            );

            //build the complete search result
            resultPartial = _.template(
                templateResult,
                {
                    id       : (key + 1),
                    title        : result.title,
                    description  : getTourDescription(result, 'meta'),
                    sku          : result.sku,
                    detail       : detailPartial,
                    duration     : getTourDuration(result),
                    guestTypes   : getAllowedGuestTypes(result, true),
                    price         : result.price,
                    currency      : result.currency,
                    view         : $('#grid').hasClass('open') ? ' col-xs-6 col-sm-6 col-md-4 grid-item' : '',
                    thumbnailList: getTourThumbnail(result, 'list-img'),
                    thumbnailGrid: getTourThumbnail(result, 'grid-img')
                }
            );

            html.push(resultPartial);

        });


        $("#search-results").append(html);

    }

    function getTourThumbnail(result, imgClass) {
        if ( result.thumbnail )
            return '<img class="'+imgClass+'" src="data:image/png;base64,'+result.thumbnail+'" alt="">';

        return '<img class="nothumb '+imgClass+'" src="/img/nothumb.png" alt="">';
    }

    function getCompanyUrl(result) {

        if ( ! result.companyUrl ) {
            return '';
        }

        return '<a href="' + result.companyUrl + '" title="'+ result.title +'" alt="' + result.companyUrl + '">' + result.companyUrl + '</a>';
    }

    function getCompanyEmail(result) {

        if ( ! result.companyUrl ) {
            return '';
        }

        return '<a href="mailto:' + result.companyEmail + '" title="'+ result.companyEmail +'" alt="' + result.companyEmail + '">' + result.companyEmail + '</a>';
    }

    function getCompanyLogo(result) {

        if ( ! _.isNull(result.companyLogo) )
            return '<img src="' + result.companyLogo.replace(/(-150x[0-9]{2,4})/g, '') + '"/>';

    }

    function getTourImages(result, size) {

        if ( size === 'large ')
            return result.images[0].replace(/(-150x150)/g, '');

        return result.images[0].replace(/(-150x150)/g, '');
    }

    function getCarouselImages(result) {

        var html = '';

        _.each(result.images, function(image, key) {
            var active = key == 0 ? ' active' : '';
            html += '<div class="item'+active+'"><img src="'+ image.replace(/(-150x[0-9]{2,4})/g, '') + '"/></div>';
        });

        return html;
    }

    function hasImages(result) {
        return result.images.length || false;
    }

    function getPlaceTags(result) {

        if ( ! result.places.length ) {
            return '';
        }

        var html = '';

        _.each(result.places, function(place, key){
            var coords = place.coords.split(',');
            html += '<span data-lat="'+coords[0]+'" data-lng="'+coords[1]+'"></i>' + place.name + '</span>'
        });

        return '<div class="tags places"><span>Places: </span>' + html + '</div>';
    }

    function getActivityTags(result) {

        if ( ! result.activities.length ) {
            return '';
        }

        var html = '';

        _.each(result.activities, function(activity, key){
            html += '<span></i>' + activity + '</span>'
        });

        return '<div class="tags activities"><span>Activities: </span>' + html + '</div>';

    }

    /////////////
    // MAP API //
    /////////////

    function initializeMap() {

        L.mapbox.accessToken = 'pk.eyJ1IjoibmV1cmxvZ2ljIiwiYSI6ImNpaGR1cGo1czBjNGh2ZmtsdmhjbXAwajQifQ.hDKG3NXNxxsGsvj1PvWPyQ';
        map = L.mapbox.map('map')
            .setView([40.73, -74.011], 9)
            .addLayer(L.mapbox.tileLayer('mapbox.light'));


        //mapboxgl.accessToken = 'pk.eyJ1IjoibmV1cmxvZ2ljIiwiYSI6ImNpaGR1cGo1czBjNGh2ZmtsdmhjbXAwajQifQ.hDKG3NXNxxsGsvj1PvWPyQ';
        //map = new mapboxgl.Map({
        //    container: 'map',
        //    style: 'mapbox://styles/neurlogic/cihi07bsy00rurom42kkoi41',
        //    center: [-74.50, 40],
        //    zoom: 9
        //});

        tourLayer = L.mapbox.featureLayer()
            .on('click', onMarkerClick)
            .on('mouseover', function(e) {
                console.log('tourlayer/marker mouseover');
                e.layer.openPopup();
            })
            .on('mouseout', function(e) {

                console.log('tourlayer/marker mouseout, e is %o',e);
                console.log('tourlayer/marker mouseout, container is %o',e.layer._popup._container);

                // logic to allow someone to mouseover the popup without having it disappear
                setTimeout(function() {
                    console.log('set timeout');
                    if(!mouseInBounds(e.layer._popup._container, e.originalEvent)) {
                        console.error('marker mouseout, out of popup bounds, closing popup');
                        e.layer.closePopup();
                    }
                }, 175);
            })
            .addTo(map);
    }

    function getSearchMarkerBySku(sku) {
        return _.find(searchMarkers, {sku: sku});
    }

    function onMarkerClick(e) {

        console.log('marker click: %o', e);

        // this seems distracting, making people miss the tour detail update in the sidebar
        // map.panTo(e.layer.getLatLng());

        hideTourDetails();
        showTourDetailsBySku(e.layer.feature.properties.tourData.sku);
    }

    /**
     * Keep track of markers position at the exact same latitude and longitude
     * coordinates so that the markers may be spiderfied after the search
     * has fully resolved.
     *
     * @param {[type]} marker [description]
     */
    function setSameLocationMarkers(marker) {

        return;

        var key = marker.position.lat().toString() + marker.position.lng().toString();

        if ( _.isEmpty(sameLocationMarkers) )
            sameLocationMarkers[key] = [];

        // console.log(key);

        if ( ! _.isUndefined(key) )
            sameLocationMarkers[key].push(marker);

    }

    function getMarkerImage() {
        return   '<div class="map-marker">'
            + '<img src="img/Map-Marker.png" class="marker-image" />'
            + '</div>';
    }

    function getClusterMarkerImage() {
        return   '<div class="cluster-marker">'
            + '<img src="img/Cluster-Marker.png" class="marker-image"/>'
            + '<label class="marker-label">{!sum}xxx</label>'
            + '</div>';
    }

    function buildGeoMarker(latlng, result) {

        // HERE - custom marker and tooltip
        // TODO - fix issue where mouseout triggers on internal popup content


        /*
         documentation seems a bit sparse for what the meaningful geojson properties are. Here's stuff from examples :

         - properties follow spec for L.mapbox.simplestyle.style.
         - See https://github.com/mapbox/simplestyle-spec/tree/master/1.1.0

         - icon follow spec for
         "icon": {
         "iconUrl": "/mapbox.js/assets/images/astronaut1.png",
         "iconSize": [50, 50], // size of the icon
         "iconAnchor": [25, 25], // point of the icon which will correspond to marker's location
         "popupAnchor": [0, -25], // point from which the popup should open relative to the iconAnchor
         "className": "dot"
         }
         */

        return {
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [
                    latlng.lat,
                    latlng.lng
                ]
            },
            "properties": {
                tourData : result,
                'marker-size': 'medium',
                'stroke' : '#CE0000',
                'marker-color' : '#CE0000',
                'marker-symbol' : 'circle'
            }
        };
    }

    function removeMapMarkers() {

        _.each(searchMarkers, function(marker, key){
            marker.setMap(null);
        });

        searchMarkers.length = 0;
    }

    function showSearchResultMarkers() {

        var results = getResults();

        if ( ! results.length ) {
            return;
        }

        var bounds = new L.latLngBounds(
                new L.latLng(0,0),
                new L.latLng(0,0)
            ),
            markers = [];

        _.each(results, function(result, key){

            console.log('show markers, result: %o', result);

            if ( _.isEmpty(result.places) ) {
                return;
            }

            var primaryPlace = result.places[0].coords.split(',');
            var placeCoords = new L.latLng(primaryPlace[1], primaryPlace[0]);

            //setMarker(placeCoords, result);

            markers.push(buildGeoMarker(placeCoords, result));

            // TODO
            // setMarkerClusters();

            bounds.extend(placeCoords);
        });

        tourLayer.setGeoJSON(markers);


        tourLayer.eachLayer(function(layer) {

            console.log('eachLayer, layer is %o',layer);

            var container = buildTooltip(layer.feature.properties.tourData);

            container.addEventListener('click', function(e) {
                console.log('popup click');
            });

            container.addEventListener('mouseover', function(e) {
                console.log('popup content mouseover');
                console.log('popup content mouseover %o',e);
            });

            container.addEventListener('mouseout', function(e) {
                console.log('popup content mouseout %o',e);

                // safety limit of 5
                var tooltip = getParentByClass(container,'leaflet-popup');
                if(!tooltip) {
                    console.error('popup not found!');
                    layer.closePopup();
                }

                if(!mouseInBounds(tooltip, e)) {
                    console.warn('popup mouseout, out of bounds, closing popup');
                    layer.closePopup();
                }
            });

            layer.bindPopup(container, {
                closeButton : false,
//                maxWidth : 400,
                minWidth : 250,
                className : 'spot-popup'
            });
        });

        map.fitBounds(bounds, {padding: [50,50]});
        //showSameLocationMarkers();
    }

    function buildTooltip(tourData) {

        // funky margin/padding is for mousein event on popup
        var container = document.createElement('DIV');
        container.id = 'tooltip-'+tourData.sku;
        container.style.margin = '-10px -10px -15px';
        container.style.padding = '10px 10px 15px';

        var img = document.createElement('IMG');
        img.src = 'data:image/jpen;base64, '+tourData.thumbnail;
        img.style.float = 'left';
        img.style.maxHeight = '75px';
        container.appendChild(img);

        var clearfix = document.createElement('DIV');
        clearfix.style.clear = 'both';
        container.appendChild(clearfix);

        var contentNode = document.createElement('DIV');

        var titleNode = document.createTextNode(tourData.title);
        contentNode.appendChild(titleNode);

        container.appendChild(contentNode);

        return container;
    }

    function hasClass(el, selector) {
        var className = " " + selector + " ";

        if ((" " + el.className + " ").replace(/[\n\t]/g, " ").indexOf(className) > -1) {
            return true;
        }

        return false;
    }

    function getParentByClass(el, className, maxDepth) {

        maxDepth = maxDepth || 5;

        var targetParent = null,
            parent = el.parentNode;

        for(var i=0; i<maxDepth && parent; i++) {
            if(hasClass(parent, className)) {
                targetParent = parent;
                break;
            }
            parent = parent.parentNode;
        }

        return targetParent;
    }

    function mouseInBounds(el, e) {

        console.log('mouseInBounds, el is %o',el);
        if(!el) {
            console.log('mouseInBounds, no el');
            return false;
        }

        var bounds = el.getBoundingClientRect();
        if(e.pageX < bounds.left || e.pageX > bounds.right || e.pageY < bounds.top || e.pageY > bounds.bottom) {
            return true;
        }

        return false;
    }

    /**
     * Submit the search for the query term supplied by the user or in which is passed
     * as an argument to the function.
     *
     * @param  {String} query Optional - the query term to search form.
     * @return {[type]}       [description]
     */
    function submitSearch(query) {

        if ( query ) {

            $('#sidebar input[name="query"]').val(query);

        } else {

            query = $('#sidebar input[name="query"]').val();

            if ( window.location.search ) {
                window.location.search = 'q='+query;
            }

        }


        if ( noSubmit || ! query ) {
            return;
        }

        $('#search-results').empty();

        hideTourDetails();
        //removeRouteMarkers();
        removeMapMarkers();

        function beforeSend(response) {
            $('#preloader').fadeIn();
        }

        function success(response) {

            response = JSON.parse(response);

            $('#preloader').fadeOut(function(){
                setResults(response);      //make result set available to app scope
                showSearchResults();       //show the search results in sidebar
                showSearchResultMarkers(); //show the search result markers on map
            });
        }

        console.log('api url is '+apiUrlSearch);
        $.ajax({
            url: apiUrlSearch,
            data: {
                q : $('#sidebar input[name="query"]').val(),
                sku    : getQueryVar('sku'),
                loc    : getQueryVar('loc')
            },
            beforeSend: beforeSend,
            success: success
        });
    }

    /**
     * Handle query variables.
     *
     * @return {[type]} [description]
     */
    function handleWindowLocation() {

        var q,
            sku,
            loc;

        if ( window.location.search ) {

            if ( q = getQueryVar('q') ) {

                submitSearch(q);

            } else if ( sku = getQueryVar('sku') ) {

                submitSearch(sku);

            } else if ( loc = getQueryVar('loc') ) {

                submitSearch(loc);

            }

        }

    }

    function getQueryVar(variable) {
        var query = window.location.search.substring(1);
        var vars = query.split('&');
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split('=');
            if (decodeURIComponent(pair[0]) == variable) {
                return decodeURIComponent(pair[1]);
            }
        }

        return '';
    }

    $(window).load(function(){

        initializeMap();



    });


    //document ready
    $(function() {

        $('#filters').click(function(){
            $('.advanced-search').slideToggle("slow");
        })


        /////////////////////
        // EVENT LISTENERS //
        /////////////////////

        $('.navbar').on('click', function(){

            //removeRouteMarkers();
            hideTourDetails();

            $.each($('.tour-details'), function(){
                $(this).hide();
            });

        });

        $('#price-range').noUiSlider({
            start: [ 0, 1000 ],
            step: 5,
            behaviour: 'drag',
            connect: true,
            range: {
                'min':  50,
                'max':  1000
            }
        });

        function lowerFormat(value) {
            $('#price-range-min').text(value).attr('data-min', value);;
        }

        function upperFormat(value) {

            if ( value === 1000 ) {
                value = value + '+';
            }

            $('#price-range-max').text(value).attr('data-max', value);

        }

        $('#price-range').Link('lower').to(lowerFormat, null, {to: parseInt, from: Number});
        $('#price-range').Link('upper').to(upperFormat, null, {to: parseInt, from: Number});

        var datePickerStart = new Pikaday({
            field: document.getElementById('date-start'),
            format: 'D MMM YYYY'
        });

        var datePickerEnd = new Pikaday({
            field: document.getElementById('date-end'),
            format: 'D MMM YYYY'
        });

        $('#grid').on('click', function(event){
            event.preventDefault();
            $('#list').removeClass('open');
            if ( $(this).hasClass('open') ) {
                $(this).removeClass('open');
            } else {
                $(this).addClass('open');
            }


            $('div.media').addClass('col-xs-6 col-sm-6 col-md-4 grid-item');

        });

        $('#list').on('click', function(event){
            event.preventDefault();
            $('#grid').removeClass('open');
            if ( $(this).hasClass('open') ) {
                $(this).removeClass('open');
            } else {
                $(this).addClass('open');
            }
            $('div.media').removeClass('col-xs-6 col-sm-6 col-md-4 grid-item');
        });

        $(document).on('change', '#date-start', function() {

            var startDate = datePickerStart.toString('YYYY-MM-DD');
            var endDate   = datePickerEnd.toString('YYYY-MM-DD');

            var startDateObject = new Date(startDate);
            var endDateMin      = moment( startDateObject ).subtract(1, 'days');

            //set the minimum date of the date-end input
            datePickerEnd.setMinDate( endDateMin );

            //if the date-start is greater than selected date-end, clear the
            //date-end input, re-show the date-end calendar and goto the
            //current date-start value.
            if ( startDate.replace(/-/g, '') > endDate.replace(/-/g, '') ) {

                $('#date-end').val('');

                datePickerEnd.show();

                datePickerEnd.gotoDate( startDateObject );

            }
        });

        $('input').keypress(function(event) {

            if (event.which == 13) {
                submitSearch();
                return false;
            }

        });

        /**
         * Search Query
         */
        $(document).on('click', 'button#go', function(event){

            event.preventDefault();
            hideNoResultMessage();
            submitSearch();

        });

        $(document).on('click', '#no-result span', function(event){

            var altTerm = $(this);
            event.preventDefault();
            if ( ! altTerm.hasClass('term') ) {

                $('#no-result').fadeOut(function(){
                    // $('#sidebar input[name="query"]').val(altTerm.text())
                    submitSearch(altTerm.text());
                });

            }

        });

        $(document).ajaxStart(function(event){
            noSubmit = true;
        });

        $(document).ajaxStop(function(event){
            noSubmit = false;
        });

        $(document).on('click', '#search-results .media', function(){

            var sku    = $(this).data('place');
            showTourDetailsBySku(sku);

        });

        $(document).on('click', '#show-results', function(event){
            event.preventDefault();
            hideTourDetails();
        });

        $('#page-results .next').on('click', function(event){
            event.preventDefault();

            var sku;

            sku    = getNextResult();

            hideTourDetails();

            showTourDetailsBySku(sku)

        });

        $('#page-results .prev').on('click', function(event){
            event.preventDefault();

            var sku,
                marker;

            sku    = getPrevResult();
            marker = getSearchMarkerBySku(sku);

            hideTourDetails();
            //removeRouteMarkers();

            showTourDetailsBySku(sku);
            //showRoute(marker);

        });

        handleWindowLocation();

    });

})(jQuery);

<?php

namespace App\Console;

use App\Services\PresignupMailer;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\SendCampaignEmails',
        'App\Console\Commands\MandrillQuery',
        'App\Console\Commands\OpenSync',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('send_campaign_emails')
			->hourly()
            //->everyFiveMinutes()
			->withoutOverlapping();
    }
}

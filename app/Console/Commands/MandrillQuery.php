<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class MandrillQuery extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'query_mandrill {id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Query Mandrill about a message';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    //public function __construct()
    //{
    //    parent::__construct();
    //}

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		try {
			$mandrill = new Mandrill(env('MANDRILL_SECRET'));
			$id = $this->argument('id');
			$result = $mandrill->messages->info($id);
			print_r($result);
		} catch(\Mandrill_Error $e) {
			// Mandrill errors are thrown as exceptions
			echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			// A mandrill error occurred: Mandrill_Unknown_Message - No message exists with the id 'McyuzyCS5M3bubeGPP-XVA'
			throw $e;
		}
    }
}

<?php

namespace App\Console\Commands;

use App\Services\PresignupMailer;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SendCampaignEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send_campaign_emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Campaign Emails';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mailer = new PresignupMailer();
        $mailer->scheduledSend();
    }
}

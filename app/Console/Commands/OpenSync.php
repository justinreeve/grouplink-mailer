<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OpenSync extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'open_sync {sinceDate}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sync email actions';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$d = $this->argument('sinceDate');
		if(!empty($d))
		{
			$sinceData = (new Carbon())->parse($d);
		} else {
			$sinceData = new Carbon(1,1,2016);
		}

		$mailData = DB::table('email_actions')
			->where('ts', '>', $sinceData->toDateTimeString())
			->get(['email_id', 'action_type', 'ts']);

		print_r("Fetched ".count($mailData)." email actions\n");

		foreach($mailData as $data)
		{
			switch($data->action_type)
			{
				case 'OPEN':
					$colName = 'opened_at';
					break;
				case 'CLICK':
					$colName = 'clicked_at';
					break;
				default:
					continue;
			}

			$ts = new Carbon($data->ts);

			try {

				// only want the first open/click
				$existing = DB::table('campaign_emails')->where('id', $data->email_id)->get([$colName]);
				if(empty($existing->{$colName}))
				{
					print_r("updating $colName \n");
					DB::table('campaign_emails')->where('id', $data->email_id)->update([$colName => $ts]);
				} else {
					print_r("skipping entry with existing $colName \n");
				}

			} catch(\Exception $e) {
				Log::info('Skipping mail because: '.$e->getMessage());
				continue;
			}

		}
	}
}

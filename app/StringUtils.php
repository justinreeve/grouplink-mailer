<?php

namespace App\Utils;

class StringUtils {

	const ALPHANUM = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	const TITLE = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$%&()[].;:\'"?!/-~,><';

	/**
	 * Generate a password
	 *
	 * @param int $len
	 *
	 * @return string
	 */
	public static function createPassword($len=12)
	{
		return self::randString($len);
	}

	/**
	 * Generates a not-perfectly-random, but not-practical-to-guess string
	 *
	 * @return string
	 */
	public static function randString($length, $charset=self::ALPHANUM) {
		$charactersLength = strlen($charset);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $charset[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	/**
	 * Unserialize value only if it was serialized.
	 *
	 * @param string $original Maybe unserialized original, if is needed.
	 *
	 * @return mixed Unserialized data can be any type.
	 */
	public static function maybe_unserialize($original)
	{
		if (self::is_serialized($original)) {
			return @unserialize($original);
		}

		return $original;
	}

	/**
	 * Check value to find if it was serialized.
	 *
	 * If $data is not an string, then returned value will always be false.
	 * Serialized data is always a string.
	 *
	 * @param mixed $data   Value to check to see if was serialized.
	 * @param bool  $strict Optional. Whether to be strict about the end of the string. Defaults true.
	 *
	 * @return bool False if not serialized and true if it was.
	 */
	public static function is_serialized($data, $strict = true)
	{
		// if it isn't a string, it isn't serialized
		if (!is_string($data)) {
			return false;
		}
		$data = trim($data);
		if ('N;' == $data) {
			return true;
		}
		if (strlen($data) < 4) {
			return false;
		}
		if (':' !== $data[1]) {
			return false;
		}
		if ($strict) {
			$lastc = substr($data, -1);
			if (';' !== $lastc && '}' !== $lastc) {
				return false;
			}
		} else {
			$semicolon = strpos($data, ';');
			$brace     = strpos($data, '}');
			// Either ; or } must exist.
			if (false === $semicolon && false === $brace) {
				return false;
			}
			// But neither must be in the first X characters.
			if (false !== $semicolon && $semicolon < 3) {
				return false;
			}
			if (false !== $brace && $brace < 4) {
				return false;
			}
		}
		$token = $data[0];
		switch ($token) {
			case 's' :
				if ($strict) {
					if ('"' !== substr($data, -2, 1)) {
						return false;
					}
				} elseif (false === strpos($data, '"')) {
					return false;
				}
			// or else fall through
			case 'a' :
			case 'O' :
				return (bool) preg_match("/^{$token}:[0-9]+:/s", $data);
			case 'b' :
			case 'i' :
			case 'd' :
				$end = $strict ? '$' : '';

				return (bool) preg_match("/^{$token}:[0-9.E-]+;$end/", $data);
		}

		return false;
	}

	public static function startsWith($haystack, $needle) {
		// search backwards starting from haystack length characters from the end
		return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
	}

	public static function endsWith($haystack, $needle) {
		// search forward starting from end minus needle length characters
		return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
	}

}

<?php

namespace App\Utils;

use Illuminate\Support\Facades\Log;
use Pdp\Parser;
use Pdp\PublicSuffixListManager;

class UrlUtils {

	protected static $nonUniqueDomains = [
		'facebook.com'
	];

	public static function isValidUrl($url)
	{
		return filter_var($url, FILTER_VALIDATE_URL) !== false;
	}

	public static function extractHost($url)
	{
		// return false if whitespace in string
		$url = strtolower(trim($url));

		// sometimes scrapers return urls without a scheme. Almost everything is going to be http.
		if( !preg_match('/^http/', $url) ) {
			$url = 'http://'.$url;
		}

		$mgr = new PublicSuffixListManager();
		$parser = new Parser($mgr->getList());

		try
		{
			$parsedUrl = $parser->parseUrl($url);
		} catch(\Exception $e)
		{
			return null;
		}

		// use the whole domain. Some companies have websites that are subdomains of their
		// ISPs or hosting companies.
		if(in_array($parsedUrl, self::$nonUniqueDomains))
		{
			return $parsedUrl->host->registerableDomain . $parsedUrl->path;
		}

		return $parsedUrl->host->registerableDomain;
	}
}

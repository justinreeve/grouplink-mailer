<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampaignEmail extends Model {

	protected $table = 'campaign_emails';

	public $timestamps = false;

	protected $fillable = [
		'mandrill_id',
		'campaign_id',
		'email',
		'first_name',
		'last_name',
		'send_at',
		'status',
		'sent_at',
		'deferred_at',
		'response_at',
		'hard_bounce_at',
		'soft_bounce_at',
		'unsubscribe_at',
		'marked_spam_at',
		'rejected_at',
		'bounce_reason',
		'bounce_detail'
	];
}

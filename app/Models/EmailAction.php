<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class EmailAction extends Model {

	const OPEN = 'OPEN';
	const CLICK = 'CLICK';

	protected $table = 'email_actions';

	public $timestamps = false;

	protected $errors;

	// for now, going to trust mandrill and not validate
	/*
	public static function boot()
	{
		parent::boot();

		static::saving(function (EmailAction $model) {

			$valid = $model->validate();
			if(!$valid)
			{
				return false;
			}

			return true;
		});
	}
	*/

	/**
	 * Validate attributes set on the model.
	 *
	 * @return bool
	 */
	public function validate()
	{
		$validation = Validator::make($this->getAttributes(), static::$validationRules);

		if ($validation->fails()) {
			$this->errors = $validation->messages();
			return false;
		}

		return true;
	}

	/**
	 * Check for any validation errors.
	 *
	 * @return mixed
	 */
	public function hasErrors()
	{
		return !empty($this->errors);
	}

	/**
	 * Retrieve any validation errors.
	 *
	 * @return mixed
	 */
	public function getErrors()
	{
		return $this->errors;
	}
}

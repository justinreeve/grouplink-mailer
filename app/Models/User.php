<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Request as RequestFacade;
use jjharr\Can\Can;

class User extends Model implements AuthenticatableContract,
                                    //AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, Can;

    const DEFAULT_PROFILE_IMAGE_URL = '/img/profile/default-avatar.png';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email', 'password', 'image', 'first_name', 'last_name', 'display_name', 'activation_key'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function organization()
    {
        return $this->belongsTo('App\Models\Organization');
    }

    public function redirectPath()
    {
        // todo - factor in role
        $rawProto = strtolower(RequestFacade::server('SERVER_PROTO'));
        $proto = strpos($rawProto, 'https') !== false ? 'https' : 'http';

        $server = RequestFacade::server('SERVER_NAME');
        $subdomain = $this->organization->local_subdomain;

        return sprintf("%s://%s.%s/%s", $proto, $subdomain, $server, 'dashboard');
    }
}

<?php

namespace app\Models;

class EmailStatus {

	const NOT_SUBMITTED = 'not_submitted';

	const SUBMITTED = 'submitted';
	const SCHEDULED = 'scheduled';
	const INVALID = 'invalid';
	const DEFERRED = 'deferred';

	const SENT = 'sent';

	const HARD_BOUNCE = 'hard_bounce';
	const SOFT_BOUNCE = 'soft_bounce';
	const REJECTED = 'rejected';
	const MARKED_SPAM = 'marked_spam';
	const UNSUBSCRIBED = 'unsubscribed';

	const OPENED = 'open';
	const CLICKED = 'click';
	const CONVERTED = 'converted';

	const UNKNOWN = 'unknown';

	public static function getPreDeliveryStatuses()
	{
		return [
			self::SCHEDULED,
			self::SUBMITTED,
			self::INVALID,
			self::DEFERRED
		];
	}

	public static function getSentStatuses()
	{
		return [
			self::SENT,
			self::HARD_BOUNCE,
			self::SOFT_BOUNCE,
			self::REJECTED,
			self::MARKED_SPAM,
			self::UNSUBSCRIBED,
			self::OPENED,
			self::CLICKED,
			self::CONVERTED,
		];
	}

	public static function getFailedStatuses()
	{
		return [
			self::HARD_BOUNCE,
			self::SOFT_BOUNCE,
			self::REJECTED,
			self::MARKED_SPAM,
			self::UNSUBSCRIBED,
		];
	}

	public static function getSuccessStatuses()
	{
		return [
			self::OPENED,
			self::CLICKED,
			self::CONVERTED,
		];
	}

	/**
	 * Important function for determining whether the status of an email should be changed. Since
	 * webhooks can be called out of order, since an email can have multiple opens or clicks, etc.
	 * It's important to use this as a check whenever updating email status.
	 *
	 * @param string $newStatus
	 * @param string $oldStatus
	 *
	 * @return bool
	 */
	public static function isHigherPriority($newStatus, $oldStatus=null)
	{

		// any status trumps no status
		// this catches most statuses - spam, bounce, reject etc.
		// because there will be no prior status
		if(!$oldStatus)
			return true;

		// These in order of descending priority

		else if($newStatus == self::CONVERTED && $oldStatus != self::CONVERTED)
			return true;

		if($newStatus == self::CLICKED && !($oldStatus == self::CLICKED || $oldStatus == self::CONVERTED))
			return true;

		// open trumps everything but clicks and other opens
		else if($newStatus == self::OPENED && !in_array($oldStatus, self::getSuccessStatuses()))
			return true;

		else if(in_array($newStatus, static::getFailedStatuses()) && ($oldStatus == self::SENT || in_array($oldStatus, static::getPreDeliveryStatuses())))
			return true;

		else if($newStatus == self::SENT && in_array($oldStatus, static::getPreDeliveryStatuses()))
			return true;

		else if($newStatus == self::DEFERRED && ($oldStatus == self::SUBMITTED || $oldStatus == self::SCHEDULED))
			return true;

		else if($newStatus == self::SUBMITTED && $oldStatus == self::SCHEDULED)
			return true;

		return false;
	}
}

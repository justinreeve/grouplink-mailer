<?php

namespace App\Models;

class EmailClick extends EmailAction {

	protected $fillable = [
		'campaign_id',
		'action_type',
		'email_id',
		'ipaddr',
		'ts',
		'location',
		'ua_raw',
		'ua_parsed',
		'url',
	];

	// for now, going to trust mandrill and not validate
	protected static $validationRules = [
		'email_id' => 'required|string|min:32',
		'ipaddr' => 'ip',
		'ts' => 'numeric',
		'url' => 'url',
	];

	public function __construct(array $attributes=[])
	{
		$attributes['action_type'] = EmailAction::CLICK;
		parent::__construct($attributes);
	}
}

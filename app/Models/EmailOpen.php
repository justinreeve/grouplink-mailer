<?php

namespace App\Models;

class EmailOpen extends EmailAction {

	protected $fillable = [
		'campaign_id',
		'action_type',
		'email_id',
		'ipaddr',
		'ts',
		'location',
		'ua_raw',
		'ua_parsed',
	];

	protected static $validationRules = [
		'message_id' => 'required|string|min:32',
		'ipaddr' => 'ip',
		'ts' => 'numeric',
	];

	public function __construct(array $attributes=[])
	{
		$attributes['action_type'] = EmailAction::OPEN;
		parent::__construct($attributes);
	}
}

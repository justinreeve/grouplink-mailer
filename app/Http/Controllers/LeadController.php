<?php

namespace App\Http\Controllers;

use App\Helpers\Leads\LeadImport;
use App\Models\CampaignEmail;
use App\Models\EmailStatus;
use Carbon\Carbon;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class LeadController extends Controller {

	public function getUploadLeads()
	{
		$total = DB::table('campaign_emails')->count();

		return view('upload-leads')->with([
			'total' => $total
		]);
	}

	public function postUploadLeads(LeadImport $import)
	{
		ini_set('memory_limit', '1G');
		set_time_limit(180);

		$handler = $import->handleImport();
		if($handler->hasErrors())
		{
			return response()->json([
				'success' => false,
				'errors' => $handler->getErrors()
			], 422);
		}

		return response()->json([
			'success' => true
		]);
	}

	public function getUnsubscribeOrg($mailHash)
	{
		$msg = 'We weren\'t able to find your email in our records. Please contact us at jnemrow@grouplink.net to unsubscribe';
		$v = Validator::make(['id' => $mailHash], ['id' => 'required|string']);
		if($v->fails())
		{
			Log::info('Unsubscribe request with invalid user id: \''.print_r($mailHash).'\'');
			return view('unsubscribed')->with(['msg' => $msg]);
		}

		$now = Carbon::now();

		// also mark email as unsubscribed for campaign analytics
		$email = CampaignEmail::where('hash', $mailHash)->first();
		if(EmailStatus::isHigherPriority(EmailStatus::UNSUBSCRIBED, $email->status))
		{
			$email->update([
				               'status' => EmailStatus::UNSUBSCRIBED,
			                   'unsubscribe_at' => $now->toDateTimeString()
			               ]);
		}

		return view('unsubscribed')->with(['msg' => 'We\'re sorry we sent you mail you didn\'t want. You\'ve been unsubscribed and we won\'t send you any more email.']);
	}
}

<?php

namespace app\Http\Controllers;

use App\Models\CampaignEmail;
use App\Models\Countries;
use App\Models\EmailAction;
use App\Models\EmailStatus;
use App\Models\Presignup;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CampaignController extends Controller {

	public function getCampaignAnalytics($campaignId)
	{
		// total number of presignups
		$presignupCount = $this->getPresignupCount();
		Log::info('presignup count is '.$presignupCount);

		$referralCount = $this->getReferralCount();

		// for pie chart of presignup provider types
		$presignupTypes = $this->getPresignupTypes();
		Log::info('presignup types is '.print_r($presignupTypes, true));

		$funnelData = $this->getCampaignData();

		$presignupRegions = $this->getPresignupRegions();

		//$percentOpensByDaySent = '';
		//$percentOpensByDayOpened = '';
		//$percentOpensByTimeSent = '';
		//$percentOpensByTimeOpened = '';

		$rep = DB::table('campaign_emails')
			->where('campaign_id', 1)
			->whereIn('status', EmailStatus::getFailedStatuses())
			->select('status', DB::raw('count(*) as total'))
			->groupBy('status')
			->orderBy('status', 'ASC')
			->get();
		Log::info('spam query result is '.print_r($rep, true));

		$totalSpam = $rep[0]['total'];
		$totalUnsubscribe = $rep[1]['total'];

		return view('campaign-analytics')->with([
			'presignupCount' => $presignupCount,
			'referralCount' => $referralCount,
			'totalSpam' => $totalSpam,
			'totalUnsubscribe' => $totalUnsubscribe,
			'presignupTypes' => $presignupTypes,
			'presignupRegions' => $presignupRegions,
			'funnelData' => $funnelData,
		]);
	}

	protected function getPresignupCount()
	{
		return DB::table('presignups')->count();
	}

	protected function getPresignupTypes()
	{
		$data = DB::table('presignups')
			->select('type', DB::raw('count(*) as total'))
			->groupBy('type')
			->get();

		$sum = 0;
		foreach($data as $item)
		{
			$sum += $item->total;
		}

		$seriesData = [];
		foreach($data as $item)
		{
			$seriesData[] = [
				'name' => $item->type,
				'y' => $item->total,
				'percentage' => $item->total/$sum,
			];
		}

		return [
			'chart' => [
				'plotBackgroundColor' => null,
				'plotBorderWidth' => null,
				'plotShadow' => false,
				'type' => 'pie',
			],
			'title' => [
				'text' => 'Presignups by Organization Type'
			],
			'tooltip' => [
				'pointFormat' => '{series.name}: <b>{point.percentage:.1f}%</b>'
			],
			'plotOptions' => [
				'pie' => [
					'allowPoinSelect' => true,
					'cursor' => 'pointer',
					'dataLabels' => [
						'enabled' => true,
						'format' => '<b>{point.name}</b>: {point.percentage:.1f} %',
						'style' => [
							'color' => "(Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'"
						]
					]
				]
			],
			'series' =>[
				'name' => 'Types',
				'colorByPoint' => true,
				'data' => $seriesData
			]
		];
	}

	protected function getSent()
	{
		return CampaignEmail::where('campaign_id',1)->count();
	}

	protected function getReferralCount()
	{
		return DB::table('presignups')->whereNotNull('referred_by')->count();
	}

	protected function getCampaignData()
	{
		$emailData = DB::table('campaign_emails')
			->whereIn('status', EmailStatus::getSentStatuses())
			->select('status', DB::raw('count(*) as total'))
			->groupBy('status')
			->get();


		$sum = 0;
		foreach($emailData as $item)
		{
			$sum += $item->total;
		}

		Log::info('getCampaignData, statusData is '.print_r($emailData, true));

		$seriesData = [];
		foreach($emailData as $item)
		{
			$seriesData[] = [
				title_case($item->status),
				$item->total,
			];
		}

		return [
			'chart' => [
				'type' => 'funnel',
			],
			'title' => [
				'text' => 'Conversion Funnel'
			],
			'tooltip' => [
				'pointFormat' => '{series.name}: <b>{point.percentage:.1f}%</b>'
			],
			'plotOptions' => [
				'series' => [
					'dataLabels' => [
						'enabled' => true,
						'format' => '<b>{point.name}</b> ({point.y:,.0f})',
						'softConnector' => true,
					],
					'neckWidth' => '30%',
					'neckHeight' => '25%',
				]
			],
			'legend' => [
				'enabled' => false
			],
			'series' =>[
				'name' => 'Presignup Campaign',
				'colorByPoint' => true,
				'data' => $seriesData // todo - array of arrays, each array with label, num
			]
		];
	}

	protected function getPresignupRegions()
	{
		$presignups = DB::table('presignups')->get(['country']);

		$regions = [];
		foreach($presignups as $user)
		{
			$region = Countries::getRegionNameByCCN($user->country);
			if(empty($region))
			{
				Log::info('No region found for country '.$user->country);
				continue;
			}

			if(!array_key_exists($region, $regions))
			{
				$regions[$region] = 0;
			}

			$regions[$region]++;
		}

		return $regions;
	}
}

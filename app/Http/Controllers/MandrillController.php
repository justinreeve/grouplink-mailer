<?php

namespace App\Http\Controllers;

use App\Models\CampaignEmail;
use App\Models\EmailClick;
use App\Models\EmailOpen;
use App\Models\EmailStatus;
use Carbon\Carbon;
use EventHomes\Api\Webhooks\MandrillWebhookController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class MandrillController extends MandrillWebhookController {

	public function handleSend($payload)
	{
		//Log::info('Send for '.$payload['msg']['email'].' : '.print_r($payload, true));

		try
		{
			$time = Carbon::createFromTimestamp($payload['ts']);
			CampaignEmail::where('mandrill_id', $payload['_id'])->update([
				'status' => EmailStatus::SENT,
				'sent_at' => $time->toDateTimeString(),
			]);
		} catch(\Exception $e) {
			Log::info('handleSend failed with message: '.$e->getMessage());
		}

		return response();
	}

	/**
	 * Handle full mailbox, offline server, message-too-large.
	 *
	 * @param $payload
	 *
	 * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
	 */
	public function handleHardBounce($payload)
	{
		//Log::info('Hard bounce for '.$payload['msg']['email'].' : '.print_r($payload, true));

		try
		{
			$time = Carbon::createFromTimestamp($payload['ts']);
			CampaignEmail::where('mandrill_id', $payload['_id'])->update([
				'status' => EmailStatus::HARD_BOUNCE,
				'hard_bounce_at' => $time->toDateTimeString(),
				'bounce_reason' => $payload['msg']['bounce_description'],
				'bounce_detail' => $payload['msg']['diag']
			]);
		} catch(\Exception $e) {
			Log::info('handleHardBounce failed with message: '.$e->getMessage());
		}

		return response();
	}

	/**
	 * Handle non-existent email address, non-existent domains, recipients who have blocked
	 * sender.
	 *
	 * @param $payload
	 *
	 * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
	 */
	public function handleSoftBounce($payload)
	{
		//Log::info('Soft bounce for '.$payload['msg']['email'].' : '.print_r($payload, true));

		try
		{
			$time = Carbon::createFromTimestamp($payload['ts']);
			CampaignEmail::where('mandrill_id', $payload['_id'])->update([
				'status' => EmailStatus::SOFT_BOUNCE,
				'soft_bounce_at' => $time->toDateTimeString(),
				'bounce_reason' => $payload['msg']['bounce_description'],
				'bounce_detail' => $payload['msg']['diag']
			]);
		} catch(\Exception $e) {
			Log::info('handleSoftBounce failed with message: '.$e->getMessage());
		}

		return response();
	}

	public function handleOpen($payload)
	{
		//Log::info('Open for '.$payload['msg']['email'].' : '.print_r($payload, true));

		try
		{
			$time = Carbon::createFromTimestamp($payload['msg']['opens'][0]['ts']);
			$email = CampaignEmail::where('mandrill_id', $payload['_id'])->first();

			if(empty($email))
			{
				Log::warning('handleOpen, No matching email found for mandrill id '.print_r($payload['_id'], true));
				return response();
			}

			// only record time for the first open
			if(EmailStatus::isHigherPriority(EmailStatus::OPENED, $email->status))
			{
				$email->update([
					'status' => EmailStatus::OPENED,
					'opened_at' => $time->toDateTimeString(),
				]);
			}

			$created = EmailOpen::create([
				'campaign_id' => 1,
				'email_id' => $email->id,
				'ipaddr' => $payload['ip'],
				'ts' => $time->toDateTimeString(),
				'location' => json_encode($payload['location']),
				'ua_raw' => $payload['user_agent'],
				'ua_parsed' => json_encode($payload['user_agent_parsed']),
			]);

			if(!$created)
			{
				Log::warning('Unable to create open event for msg with Mandrill id: '.$payload['_id']);
			}

		} catch(\Exception $e) {
			Log::info('handleOpen failed with message: '.$e->getMessage());
		}

		return response();
	}

	public function handleClick($payload)
	{
		//Log::info('Click for '.$payload['msg']['email'].' : '.print_r($payload, true));

		try
		{
			$time = Carbon::createFromTimestamp($payload['msg']['clicks'][0]['ts']);
			$email = CampaignEmail::where('mandrill_id', $payload['_id'])->first();

			if(empty($email))
			{
				Log::warning('handleClick, No matching email found for mandrill id '.print_r($payload['_id'], true));
				return response();
			}

			// only record time for the first click
			if(EmailStatus::isHigherPriority(EmailStatus::CLICKED, $email->status))
			{
				$email->update([
					'status' => EmailStatus::CLICKED,
					'clicked_at' => $time->toDateTimeString(),
				]);
			}

			$created = EmailClick::create([
				'campaign_id' => 1,
				'email_id' => $email->id,
				'ipaddr' => $payload['ip'],
				'ts' => $time->toDateTimeString(),
				'location' => json_encode($payload['location']),
				'ua_raw' => $payload['user_agent'],
				'ua_parsed' => json_encode($payload['user_agent_parsed']),
				'url' => $payload['url']
			]);

			if(!$created)
			{
				Log::warning('Unable to create click event for msg with Mandrill id: '.$payload['_id']);
			}

		} catch(\Exception $e) {
			Log::info('handleClick failed with message: '.$e->getMessage());
		}

		return response();
	}

	public function handleSpam($payload)
	{
		//Log::warning('Spam for '.$payload['msg']['email'].' : '.print_r($payload, true));

		try
		{
			$time = Carbon::createFromTimestamp($payload['ts']);
			CampaignEmail::where('mandrill_id', $payload['_id'])->update([
				'status' => EmailStatus::MARKED_SPAM,
				'marked_spam_at' => $time->toDateTimeString(),
			]);

		} catch(\Exception $e) {
			Log::info('handleSpam failed with message: '.$e->getMessage());
			return response();
		}

		try{
			$mandrill = new \Mandrill(env('MANDRILL_SECRET'));

			$mandrill->messages->send([
				'text' => 'The lead with the email \''.$payload['msg']['email'].'\' marked an email as spam.',
				'subject' => 'A marketing email was marked as spam',
				'from_email' => 'email_notify@glmailer@cornercanyonconsulting.com',
				'from_name' => 'Email Notify',
				'to' => [
					[
						'email' => 'josh@globio.co',
						'name' => 'Grouplink Support',
						'type' => 'to'
					]
				],
			], false);
		} catch(\Exception $e) {
			Log::warning('spam notification failed : '.get_class($e).' and message '.print_r($e->getMessage(), true));
		}

		return response();
	}

	public function handleReject($payload)
	{
		//Log::info('Reject for '.$payload['msg']['email'].' : '.print_r($payload, true));

		try
		{
			$time = Carbon::createFromTimestamp($payload['ts']);
			CampaignEmail::where('mandrill_id', $payload['_id'])->update([
				'status' => EmailStatus::REJECTED,
				'rejected_at' => $time->toDateTimeString(),
			]);
		} catch(\Exception $e) {
			Log::info('handleReject failed with message: '.$e->getMessage());
		}

		return response();
	}

	public function handleDeferral($payload)
	{
		//Log::info('Deferral for '.$payload['msg']['email'].' : '.print_r($payload, true));

		try
		{
			$time = Carbon::createFromTimestamp($payload['ts']);
			CampaignEmail::where('mandrill_id', $payload['_id'])->update([
				'status' => EmailStatus::DEFERRED,
				'deferred_at' => $time->toDateTimeString(),
			]);
		} catch(\Exception $e) {
			Log::info('handleDeferral failed with message: '.$e->getMessage());
		}

		return response();
	}

	/*
	public function handleUnsub($payload)
	{
		Log::info('Unsubscribe for '.$payload['msg']['email'].' : '.print_r($payload, true));
		// update unsubscribe at, update lead status somehow
		return response();
	}
	*/

	// This probably only gets issued if we have our own IP, which we currently don't
	/*
	public function handleBlacklist($payload)
	{
		Log::info('Blacklist payload : '.print_r($payload, true));

		// send us an email so we know right away when this happens
		// probably want to add more information when we find out what
		// the payload looks like.

		try{
			$mandrill = new \Mandrill(env('MANDRILL_SECRET'));

			$mandrill->messages->send([
				'text' => 'Check log for payload details',
				'subject' => 'We\'ve been blacklisted',
				'from_email' => 'email_notify@globio.co',
				'from_name' => 'Email Notify',
				'to' => [
					[
						'email' => 'support@globio.co',
						'name' => 'Globio Support',
						'type' => 'to'
					]
				],
			], false);
		} catch(\Exception $e) {
			Log::warning('blacklist notification failed : '.get_class($e).' and message '.print_r($e->getMessage(), true));
		}

		return response();
	}
	*/

	/*
	public function handleWhitelist($payload)
	{
		Log::info('Whitelist for '.$payload['msg']['email'].' : '.print_r($payload, true));
		return response();
	}
	*/
}

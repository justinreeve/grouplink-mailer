<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Route;


Route::post('monkeyhook', ['as' => 'mandrill.webhook', 'uses' => 'MandrillController@handleWebHook']);
Route::get('/upload-leads', 'LeadController@getUploadLeads');
Route::post('/upload-leads', 'LeadController@postUploadLeads');
Route::get('unsubscribe/{userId}', 'LeadController@getUnsubscribeOrg');

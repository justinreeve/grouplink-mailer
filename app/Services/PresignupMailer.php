<?php

namespace App\Services;

use App\Models\Countries;
use App\Models\EmailStatus;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PresignupMailer {

	protected $maxEmailsPerHour = 1000;

	protected $mandrill;

	protected $subjects = [
		'template1' => 'priority on the Support request?',
		'template2' => 'priority on the Support request?',
		'template3' => 'cost on the Support request?',
		'template4' => 'cost on the Support request?',
		'template5' => 'priority on the Support request?',
		'template6' => 'priority on Support request?    ...help me Obi Wan...',
		'template7' => 'priority on Support request?    ...help me Obi Wan...',
		'template8' => 'a better helpdesk approach? - or Jedi mind tricks',
		'template9' => 'a better helpdesk approach?',
		'template10' => 'a better helpdesk approach? - or Jedi mind tricks',
		'template11' => 'priority on Support request?',
		'template12' => 'priority on Support request?   help me Obi-Wan',
		'template13' => 'a pesky support request? – or a Jedi mind trick',
		'template14' => 'priority on Support request? - or a Jedi mind trick',
		'template15' => 'top 3 budget busters from K-12 facilities',
		'template16' => 'district budget red flags',
		'template17' => 'top 3 K-12 bldg woes',
		'template18' => 'top 3 school bldg woes',
		'template19' => 'top 3 bldg woes for K-12',
		'template20' => 'top 3 K-12 facility savings',
		'template21' => 'top 3 K-12 savings in facilities',
		'template22' => 'top 3 school facility savings',
		'template23' => 'top 3 school facility savings',
		'template24' => 'top 3 K-12 savings in facilities',
		'template25' => 'please fwd: report on school facility savings best practices',
		'template26' => 'priority on Support request? – or a Jedi mind trick',
		'template27' => 'please fwd: top 3 school facility savings - -',
		'template28' => 'please fwd: top 3 K-12 savings in facilities - -',
		'template29' => 'please fwd: report on school facility savings - -',
		'template30' => 'please fwd: top 3 school facility savings',
		'template31' => 'please fwd: top 3 K-12 savings in facilities',
		'template32' => 'please fwd: report on school facility savings',
		'template33' => 'please fwd: top 3 K-12 savings in facilities',
		'template34' => 'please fwd: top 3 school facility savings',
	];


	public function __construct()
	{
		$this->mandrill = new \Mandrill( env('MANDRILL_SECRET'));
	}

	/*
	 * Merge Vars:
	 *
	 * https://mandrill.zendesk.com/hc/en-us/articles/205582537-Using-Handlebars-for-Dynamic-Content
	 */

	public function scheduledSend()
	{
		$listData = $this->getMailData();

		if(empty($listData))
		{
			return;
		}

		// batch by sending time
		$batches = [];
		foreach($listData as $data)
		{
			if(empty( $batches[$data->send_at] ))
			{
				$batches[$data->send_at] = [];
			}

			$batches[$data->send_at][] = $data;
		}

		Log::info('### scheduledSend, batches: ###');
		foreach($batches as $time => $batch)
		{
			Log::info("    @ $time : ");
			foreach($batch as $item)
			{
				Log::info("        {$item->email}");
			}
		}

		foreach($batches as $sendAt => $batchData)
		{
			$this->sendBatch($batchData, $sendAt);
		}
	}

	protected function getMailData()
	{
		// add some extra minutes because cron isn't exact
		$cutoff = Carbon::now('UTC')->addHours(4)->toDateTimeString();

		$emailData = DB::table('campaign_emails')
			->where('send_at', '<', $cutoff)
			->where('status', '=', EmailStatus::NOT_SUBMITTED)
			->orderBy('send_at', 'ASC')
			->get();

		//Log::info("===== selected emails for {$cutoff} are: =====");
		//foreach($emailData as $item)
		//{
		//	Log::info($item->email);
		//}

		return $emailData;
	}

	protected function sendBatch($emailData, $sendAt)
	{
		// for the grouplink mailer, send batch actually just sends a batch singly

		foreach($emailData as $data)
		{
			$messageData = [];
			$messageData['subject'] = $this->getSubject($data);
			$messageData['html'] = $this->getHtml($data);
			$messageData['text'] = $this->getText($data);

			$messageData['to'][] = [
				'email' => $data->email,
				'name' => $data->first_name . ' ' . $data->last_name,
				'type' => 'to'
			];

			$message = $this->buildMessage($messageData);
			$this->submitEmail($message, $sendAt, $emailData);
			sleep(1);
		}
	}

	protected function getSubject($emailData)
	{
		if(!array_key_exists($emailData->template_slug, $this->subjects))
		{
			throw new \Exception('No subject found for template '.$emailData->template_slug);
		}

		return $this->subjects[$emailData->template_slug];
	}

	protected function getHtml($emailData)
	{
		$htmlPath = 'emails.campaigns.'.$emailData->template_slug;
		if(!view()->exists($htmlPath))
		{
			throw new \Exception('email view '.$htmlPath.' doesn\'t exist');
		}

		$html = view($htmlPath)->with([
			'firstName' => $emailData->first_name,
		    'mailHash' => $emailData->hash
		])->render();
		//Log::info('buildMessage, rendered html is '.print_r($html, true));

		return $html;
	}

	protected function getText($emailData)
	{
		$textPath = 'emails.campaigns.'.$emailData->template_slug.'-text';
		if(!view()->exists($textPath))
		{
			throw new \Exception('email view '.$textPath.' doesn\'t exist');
		}

		// add ->with([]) as needed
		$text = view($textPath)->with([
              'firstName' => $emailData->first_name,
              'mailHash' => $emailData->hash
		])->render();
		//Log::info('buildMessage, rendered text is '.print_r($text, true));

		return $text;
	}

	protected function buildMessage(array $messageData)
	{
		$message = [
			'html' => $messageData['html'],
			'text' => $messageData['text'],
			'subject' => $messageData['subject'],
			'from_email' => 'jnemrow@skycentral.com',
			'from_name' => 'Joe Nemrow',
			'to' => $messageData['to'],
			//'headers' => '',
			//'important' => false,
			'track_opens' => true,
			'track_clicks' => true,
			'auto_text' => [],
			'auto_html' => false,
			'inline_css' => false,
			'url_strip_qs' => true,
			'preserve_recipients' => false,
			//'return_path_domain' => '', // https://mandrill.zendesk.com/hc/en-us/articles/205582727-How-to-Customize-the-Return-Path-Address
			'merge' => true,
			'merge_language' => 'handlebars',
			'global_merge_vars' => [],
			'merge_vars' => [],
			'tags' => [],
			//'google_analytics_domains' => '',
			//'google_analytics_campaign' => '',
			//'metadata' => [],

			// just use our own DB
			'recipient_metadata' => [],
			//'attachments' => [],
			//'images' => [],
		];

		//Log::info('buildMessage, message is '.print_r($message, true));

		return $message;
	}

	protected function submitEmail($message, $sendAt, $batchData)
	{
		try{
			//foreach($message['to'] as $to)
			//{
			//	Log::info('==> submitting email for '.print_r($to['email'], true));
			//}
			//Log::info('submitEmail, message is '.print_r($message, true));
			$response = $this->mandrill->messages->send($message, true, null, $sendAt);
			return $this->updateAfterSubmit($response, $batchData);
		} catch(\Mandrill_Error $e) {
			Log::info('send exception: '.print_r($e->getMessage(), true));
		} catch(\Exception $e) {
			Log::info('message send failed with exception : '.get_class($e).' and message '.print_r($e->getMessage(), true));
		}

		return false;
	}

	protected function updateAfterSubmit(array $mandrillResponse, array $batchData)
	{
		$qValues = $this->buildSubmittedQueryValues($mandrillResponse, $batchData);
		//Log::info('qvalues is '.print_r($qValues, true));

		$statement = sprintf("
			UPDATE campaign_emails AS x SET
				mandrill_id = y.mandrill_id,
				status = y.status,
				submitted_at = y.submitted_at
			FROM (VALUES
				%s
			) as y(email, mandrill_id, status, submitted_at)
			WHERE y.email = x.email
			", $qValues);

		$count = DB::update($statement);
		//Log::info('updateAfterSubmit, count is '.print_r($count, true));

		return $count;
	}

	protected function buildSubmittedQueryValues(array $mandrillResponse, array $batchData)
	{
		// mandrill gives us back an entry for every email. We need the organization id for that email
		// for our update. We can can do this without an extra query using the mail data we looked up earlier
		$emailIdMap = [];
		foreach($batchData as $data)
		{
			$emailIdMap[$data->email] = $data->id;
		}

		$now = Carbon::now('UTC')->toDateTimeString();

		$statusFunc = function($mStatus)
		{
			$mStatus = strtolower($mStatus);
			switch($mStatus)
			{
				// These are not our email statuses, but those that Mandrill can return
				// after a send() call. What we return our equivalent email status.
				// https://mandrillapp.com/api/docs/messages.php.html#method=send

				case 'sent':
					return EmailStatus::SENT;
				case 'scheduled':
					return EmailStatus::SCHEDULED;
				case 'rejected':
					return EmailStatus::REJECTED;
				case 'invalid':
					return EmailStatus::INVALID;
				case 'queued':
					return EmailStatus::SUBMITTED;
				default:
					Log::info('received unknown status from mandrill: '.$mStatus);
					return EmailStatus::UNKNOWN;
			}
		};

		// The Mandrill is an array of structs. Each struct contains email, status, reject_reason, _id
		$values = [];
		foreach($mandrillResponse as $record)
		{
			$email = $record['email'];
			$status = $statusFunc($record['status']);

			$values[] = sprintf("('%s', '%s', '%s', to_timestamp('%s', 'YYYY-MM-DD HH24:MI:SS'))", $email, $record['_id'], $status, $now);
		}

		$valuesStr = implode(",\n", $values);
		//Log::info('buildSubmittedQueryValues, values is '.print_r($valuesStr, true));

		return $valuesStr;
	}

	/*
	protected function buildRecipientLists(array $listData, callable $extractionFunc, $listDescription)
	{
		$recipientList = [];

		foreach($listData as $mailData)
		{
			$templateName = $this->selectTemplate($listDescription);

			if(!isset($recipientList[$templateName]))
			{
				$recipientList[$templateName] = [];
			}

			$recipientList[$templateName][] = $extractionFunc($mailData);
		}

		return $recipientList;
	}
	*/

	/**
	 * Returns an array in the form of [%probabillity => template1, %probability => template2, ...]
	 *
	 * Individiual probabilities are <= 1, and are added to make binning easier. So if there are 4
	 * templates, the returned probabilities would be 0.25, 0.5, 0.75, 1
	 *
	 * @param array $templateNames
	 * @param array $templateShares
	 *
	 * @return array
	 * @throws \Exception
	 */
	/*
	protected function buildListDescription(array $templateNames, array $templateShares=null)
	{
		if(empty($templateNames))
		{
			throw new \Exception("Template list must have at least one entry");
		}

		$templateCount = count($templateNames);
		$description = [];

		if(empty($templateShares))
		{
			$equalShare = 1/$templateCount;
			for($i=1; $i <= count($templateNames); $i++)
			{
				$description[ $i * $equalShare ] = $templateNames[$i];
			}
		} else {
			if($templateCount != count($templateShares))
			{
				throw new \Exception("There must be equal numbers of template names and template shares.");
			}

			$sum = array_sum(array_values($templateShares));
			if($sum != 1)
			{
				throw new \Exception("The sum of the template shares must add to 1. They current add to ".$sum);
			}


			for($i=0; $i<$templateCount; $i++)
			{
				$description[ $templateShares[$i] ] = $templateNames[$i];
			}
		}

		return $description;
	}

	protected function selectTemplate(array $listDescription)
	{
		$roll = rand(0,1);
		foreach($listDescription as $prob => $name)
		{
			if($roll <= $prob)
			{
				return $name;
			}
		}

		throw new \Exception("We have a probabilyt problem in selectTemplate");
	}
	*/

	/* TODO
	protected function getTemplateName()
	{
		$rand = mt_rand(0,1);
		$count = count($this->templates);
		$interval = 1/$count;

		for($i=0, $j=$interval; $i<$count; $i++, $j+=$interval)
		{
			if($rand < $j)
			{
				return $this->templates[$i];
			}
		}

		Log::info('Some bad template logic in getTemplateName');
		return $this->templates[0];
	}
	*/
}

<?php

namespace App\Helpers\Leads;


use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Files\ExcelFile;

class LeadImport extends ExcelFile {

	const UPLOAD_KEY = 'leads';

	// relative to /public dir
	const TMP_PATH = '../storage/app/leads';

	protected static $moveTo = false;

	public function getFile()
	{
		if(!self::$moveTo)
		{
			$file = Input::file(self::UPLOAD_KEY);
			$file->move(self::TMP_PATH, $file->getClientOriginalName());
			self::$moveTo = self::TMP_PATH . DIRECTORY_SEPARATOR . $file->getClientOriginalName();
		}

		return self::$moveTo;
	}

	public function getFilters()
	{
		/*
		 * BUG :
		 *
		 * example @ http://www.maatwebsite.nl/laravel-excel/docs/import#chunk says that this should just be
		 * the string 'chunk'. However, if you do that, then LaravelExcelReader::_enableFilters fails where
		 * it's looking for key => class
		 *
		 * However if you set this to 'chunk' => 'Maatwebsite\Excel\Filters\ChunkReadFilter', then the code
		 * fails at LaravelExcelReader::chunk where it's just looking for the value 'chunk'
		 *
		 * The code should probably not require the key => val here because the filter should already have
		 * been registered. Instead, _enableFilters should be doing some kind of lookup against the name.
		 *
		 * More :
		 *
		 * $this->filters['registered'] should be key => class
		 * $this->filters['enabled'] should be array of keys
		 *
		 * So problem is that somehow the value from this function is making its way into registered, which
		 * it shouldn't be.
		 *
		 * To be more exact, ExcelFile::loadFilters is using this function to register filters, and it
		 * shouldn't be because registered filters have a different format. But commenting that out doesn't
		 * fix the problem.
		 */
		return [
			//'chunk' => 'Maatwebsite\Excel\Filters\ChunkReadFilter'
			'chunk'
		];
	}

}

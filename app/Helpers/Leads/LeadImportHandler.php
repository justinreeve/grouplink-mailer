<?php

namespace App\Helpers\Leads;

use App\Utils\StringUtils;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Collections\RowCollection;
use Maatwebsite\Excel\Files\ImportHandler;

class LeadImportHandler implements ImportHandler {

	/**
	 * Format is array of messages without keys
	 *
	 * @var array
	 */
	protected $errors = [];

	public function handle($import)
	{
		$uploadedLeads = $import->get()->first();
		$this->handleMailingInsertions($uploadedLeads);
		return $this;
	}

	protected function handleMailingInsertions(RowCollection $rowCollection)
	{
		$now = Carbon::now();

		$mailings = [];
		$rowNum = 2;
		foreach($rowCollection as $cellCollection)
		{
			$item = $cellCollection->all();

			if(empty($item['send_day']))
			{
				continue;
			}

			$validator = Validator::make($item, [
				'first_name' => 'required|string',
				'last_name' => 'required|string',
			    'email' => 'required|email',
			    'template' => 'required|alphanum|min:8'
			]);

			if($validator->fails())
			{
				$this->addItemValidationErrors($rowNum, $validator->messages());
			} else {

				$item['send_at'] = $this->normalizeDateTime($item['send_day'], $item['send_time'], $item['gmt_offset']);

				$mailings[] = [
					'campaign_id' => 1,
					'hash' => StringUtils::randString(4),
					'first_name' => $item['first_name'],
					'last_name' => $item['last_name'],
					'template_slug' => $item['template'],
					'email' => $item['email'],
					'send_at' => $item['send_at'],
					'created_at' => $now->toDateTimeString(),
					'updated_at' => $now->toDateTimeString()
				];
			}


			$rowNum++;
		}

		if(!empty($this->errors))
		{
			return false;
		}

		return DB::table('campaign_emails')->insert($mailings);
	}

	protected function normalizeDateTime($sendDay, $sendTime, $offset)
	{
		if(empty($sendDay))
		{
			$this->errors[] = 'Sending day and time must be set for all rows';
			return false;
		}

		if($sendDay instanceof Carbon)
		{
			$sendAt = $sendDay->startOfDay();
		} else {
			$sendAt = Carbon::createFromFormat('n/j/Y',$sendDay)->startOfDay();
		}

		// add the hours offset
		if($sendTime instanceof Carbon)
		{
			$time = $sendTime;
		} else {
			$time = Carbon::createFromFormat('G:i', $sendTime);
		}

		$sendAt->addHours($time->hour);
		$sendAt->addMinutes($time->minute);

		// because of abs, both offsets are positive after processing, so preserve sign
		$multiplier = $offset < 0 ? -1 : 1;

		// floor rounds to the larger number when negative, so have to use abs :(
		$hourOffset = floor(abs($offset));

		$minuteOffset = fmod(abs($offset), $hourOffset) * 60;

		// restore to original signs
		$hourOffset *= $multiplier;
		$minuteOffset *= $multiplier;

		// mult by -1 because we want to work backwards from UTC. If gmt offset is +1, we want to send at the
		// target time -1 hour.
		$sendAt->addHours(-1 * $hourOffset);
		$sendAt->addMinutes( -1 * $minuteOffset );

		return $sendAt;
	}

	protected function addItemValidationErrors($rowNum, $itemErrors)
	{

		foreach($itemErrors->getMessages() as $key => $errorList)
		{
			foreach($errorList as $errorMsg)
			{
				$this->errors[] = "Row $rowNum : $errorMsg";
			}
		}
	}

	public function hasErrors()
	{
		return count($this->errors) > 0;
	}

	public function getErrors()
	{
		return $this->errors;
	}
}

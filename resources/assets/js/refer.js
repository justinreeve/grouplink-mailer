
var refRegex = new RegExp("[\\?&]" + 'ref' + "=([^&#]*)");

$(document).ready(function(e) {

    var results = refRegex.exec(location.search),
        ref = results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));

    // if there isn't a ref id in the url or the form, ask them for their email
    // so we can look up the referrer and give proper credit
    if(!ref) {
        ref = $('#referrer-id').val();

        if(!ref) {
            $('#referrer-email').parents('.form-group').show();
        }
    }
});

$('#referral-form').on('submit', function(e) {

    hideAlerts();

    var token = $('meta[name="csrf-token"]').attr('content'),
        results = refRegex.exec(location.search),
        urlRef = results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' ')),
        data = {
            name: $('#name').val(),
            email: $('#email').val(),
            message: $('#message').val()
        };

    if(urlRef) {
        data['referrerId'] = urlRef;
    } else if($('#referrer-id').val()) {
        data['referrerId'] = $('#referrer-id').val();
    } else {
        data['referrerEmail'] = $('#referrer-email').val();
    }

    console.log('data is %o',data);
    $.ajax({
        url: '/refer',
        method: 'POST',
        data: data,
        headers: {
            'X-CSRF-TOKEN': token
        },
        success: function(response) {
            console.log('success %o',response);

            $('#ref-message').find('.success-text').html(response.message);
            $('#ref-message').show();

            // leave ref id and message
            $('#email').val('');
            $('#name').val('');
        },
        error: function(xhr) {
            console.log('error %o',xhr);

            var errors = null,
                errId,
                errMsg;
            try {
                errors = JSON.parse(xhr.responseText);
                for(var err in errors) {
                    errId = err + '-error';
                    errMsg = errors[err][0];

                    $('#'+errId).find('.error-text').text(errMsg);
                    $('#'+errId).show();
                }
            } catch(e) {
                $('#gen-error').find('.error-text').text('There was a problem sending your referral invitation. Sorry.').show();
            }
        }
    });

    return false;
});

function hideAlerts() {
    var alerts = $('.alert');
    alerts.find('.error-text').text('');
    alerts.find('.success-text').text('');
    alerts.hide();

}

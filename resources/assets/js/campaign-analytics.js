
var
    interactivid_clients_select = $('#interactivid-clients'),
    interactivid_clients_selectize,
    interactivid_selected_client_id,
    interactivid_projects_select = $('#interactivid-projects'),
    interactivid_projects_selectize,
    interactivid_selected_project_id,
    interactivid_analytics_daterange = $('#reportrange'),
    interactivid_analytics_daterangepicker,
    interactivid_analytics_start_date,
    interactivid_analytics_end_date,
    interactivid_query_interval;


if(interactivid_isPartner) {
    initClientSelect(interactivid_clients);
} else {
    initProjects(interactivid_projects);
}

function initClientSelect(options) {

    console.log('initClientSelect, options: %o',options);

    interactivid_clients_select.selectize({
        valueField: 'id',
        labelField: 'name',
        searchField: 'name',
        options : options,
        onInitialize : function() {

            if(!hasClients(options)) {
                return;
            }

            loadProjects(options[0].id);
        },
        render: {
            item: function(item, escape) {
                return '<div>' +
                    '<span class="name">' + escape(item.email) + '</span>' +
                    '</div>';
            },

            option: function(item, escape) {

                return '<div>' +
                    '<img source="'+item.image_url+'" height="40" width="60">' +
                    '<span class="label">' + escape(item.email) + '</span>' +
                    '</div>';
            }
        },
        onChange: function(value) {
            interactivid_selected_client_id = value;
            loadProjects(value);
        }
    });

    interactivid_clients_selectize = interactivid_clients_select['0'].selectize;
    interactivid_clients_selectize.setValue(options[0].id, true);
}

function loadProjects(clientId) {

    $.ajax({
        url : '/dashboard/video',
        method : 'GET',
        data : {
            'uid' : clientId
        },
        success: function(response) {
            onLoadProjects(response.result);
        },
        error: function(xhr) {
            // todo - show error alert
            console.error(xhr);
        }
    });
}

function hasClients(clientData) {
    return clientData.length > 1 || clientData[0].id > 0;
}

function onLoadProjects(projectData) {

    if(projectData.length == 0) {
        projectData = [{
            id: -1,
            project_name: 'There are no projects to show!'
        }];
    }

    if(interactivid_projects_selectize) {
        updateProjectSelect(projectData);
    } else {
        $('#interactivid-projects').parents('.iv-js-analytic-control').show();
        $('#reportrange').parents('.iv-js-analytic-control').show();

        initProjects(projectData);
    }
}

function initProjects(projectData) {

    initProjectSelect(projectData);
    interactivid_selected_project_id = projectData[0].id;

    // after init, doesn't need to be updated programatically?
    interactivid_analytics_daterange.daterangepicker({
        ranges: getRanges()
    }, rangepicker_cb);
    interactivid_analytics_daterangepicker = interactivid_analytics_daterange.data('daterangepicker');
    rangepicker_cb(moment().subtract(29, 'days'), moment(), 'Previous 30 Days');
}

function initProjectSelect(options) {

    interactivid_projects_select.selectize({
        valueField: 'id',
        labelField: 'project_name',

        // possibly add in array : hoster, description, company name for resellers
        searchField: 'project_name',
        options : options,
        render: {
            item: function(item, escape) {
                return '<div>' +
                    '<span class="name">' + escape(item.project_name) + '</span>' +
                    '</div>';
            },

            // could conceivably add something extra like recent plays in the select
            option: function(item, escape) {

                return '<div>' +
                    '<img source="'+item.thumbnail+'" height="40" width="60">' +
                    '<span class="label">' + escape(item.project_name) + '</span>' +
                    '</div>';
            }
        },
        load: function(query, callback) {
            console.log('selectize load');
        },
        onChange: function(value) {
            interactivid_selected_project_id = value;
            $('#postroll_chart').hide();

            getCharts();
        }
    });

    interactivid_projects_selectize = interactivid_projects_select['0'].selectize;
    interactivid_projects_selectize.setValue(options[0].id, true);

}

function updateProjectSelect(projectData) {
    interactivid_projects_selectize.clear(true);
    interactivid_projects_selectize.clearOptions();
    interactivid_projects_selectize.addOption(projectData);
    interactivid_projects_selectize.refreshOptions();

    interactivid_projects_selectize.setValue(projectData[0].id, true);
    interactivid_selected_project_id = projectData[0].id;
}

///////////////
// Daterange Picker
///////////////

var dateRanges = {
    'today' : {
        currentSummaryLabel : 'today',
        previousSummaryLabel : 'yesterday',
        pickerLabel : 'Today',
        daterange : [moment(), moment()]
    },
    'yesterday' : {
        currentSummaryLabel : 'yesterday',
        previousSummaryLabel : 'the day before yesterday',
        pickerLabel : 'Yesterday',
        daterange : [moment().subtract(1, 'days'), moment().subtract(1, 'days')]
    },
    'week' : {
        currentSummaryLabel : 'this week',
        previousSummaryLabel : 'the previous week',
        pickerLabel : 'This Week',
        daterange : [moment().startOf('week'), moment()]
    },
    'trailing7' : {
        currentSummaryLabel : 'in the previous 7 days',
        previousSummaryLabel : 'in the 7 days prior',
        pickerLabel : 'Previous 7 Days',
        daterange : [moment().subtract(6, 'days'), moment()]
    },
    'month' : {
        currentSummaryLabel : 'this month',
        previousSummaryLabel : 'last month',
        pickerLabel : 'This Month',
        daterange : [moment().startOf('month'), moment()]
    },
    'trailing30' : {
        currentSummaryLabel : 'in the previous 30 days',
        previousSummaryLabel : 'in the 30 days prior',
        pickerLabel : 'Previous 30 Days',
        daterange : [moment().subtract(29, 'days'), moment()]
    }
};

function getRanges() {
    var ranges = {},
        item;

    for(rangeKey in dateRanges) {
        item = dateRanges[rangeKey];
        ranges[ item['pickerLabel'] ] = item['daterange'];
    }

    return ranges;
}

function rangeKeyForLabel(label) {

    for(rangeKey in dateRanges) {
        item = dateRanges[rangeKey];
        if(label == item['pickerLabel']) {
            return rangeKey;
        }
    }

    return 'custom';
}

function rangepicker_cb(start, end, label) {
    interactivid_analytics_start_date = start.format('X');
    interactivid_analytics_end_date = end.format('X');
    interactivid_query_interval = rangeKeyForLabel(label);
    interactivid_analytics_daterange.find('span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    getCharts();
}

//interactivid_analytics_daterange.daterangepicker({
//    ranges: getRanges()
//}, rangepicker_cb);

///////////////
// Render Charts
///////////////

var ChartFuncs = function() {

    var area = function(selectorId, data) {
            highcharts(selectorId, data);
        },

        heatmap = function(selectorId, data) {
            $('#'+selectorId).empty();

            var tooltipKey = '';
            switch(data['dataKey']) {
                case 'play' :
                    tooltipKey = 'Views';
                    break;
                case 'action' :
                    tooltipKey = 'Viewer actions';
                    break;
                default :
                    console.error('Unknown data key: '+data['dataKey']);
                    return;
            }

            data['tooltip'] = {};
            data['tooltip']['formatter'] = function() {
                return tooltipKey + ' at ' +
                    this.series.xAxis.categories[this.point.x] +
                    ' seconds : <b>' +
                    this.point.value + '</b>';
            };

            $('#'+selectorId).highcharts(data);
        },

        bar = function(selectorId, data) {

            // hacky - for postroll chart which is only shown if project has a postroll element
            $('#'+selectorId).show();

            highcharts(selectorId, data);
        },

        summary = function(selectorId, data) {

            $('#'+selectorId).empty();

            var parentDiv = $('#'+selectorId), item, id, currentText, previousText;
            for(var i=0; i<data.length; i++) {
                item = data[i];

                id = 'summary-'+item['key'];
                parentDiv.append('<div id="'+id+'"></div>');



                currentText = ' '+item['label'];
                if(dateRanges[interactivid_query_interval]) {
                    currentText += ' ' + dateRanges[interactivid_query_interval]['currentSummaryLabel'];
                }

                $('#'+id).append('<p><span>'+item['current']+'</span>'+currentText+'</p>');

                if(item.hasOwnProperty('previous')) {
                    previousText = ' '+item['label'];
                    if(dateRanges[interactivid_query_interval]) {
                        previousText += ' ' + dateRanges[interactivid_query_interval]['previousSummaryLabel'];
                    }
                    $('#'+id).append('<p>'+item['previous']+previousText+'</p>');
                }
            }
        };

    function highcharts(selectorId, data) {
        var container = $('#'+selectorId);
        container.empty();
        container.highcharts(data);
    }

    return {
        area : area,
        bar : bar,
        heatmap : heatmap,
        summary : summary
    };
};

function getCharts(params) {

    var params = params || {};
    var projectId = params.projectId || interactivid_selected_project_id;
    var start = params.startDate || interactivid_analytics_start_date;
    var end = params.endDate || interactivid_analytics_end_date;

    // empty project
    if(projectId == -1) {
        return;
    }

    var queryParams = {
        charts : interactivid_charts, // global, set on blade template
        interval : interactivid_query_interval
    };

    $.ajax({
        url : '/dashboard/analytics/charts/'+projectId+'/'+start+'/'+end,
        method: 'GET',
        data: queryParams,
        success: function(response) {
            renderCharts(response.result.data);
        }
    });
}

function renderCharts(data) {
    var chartBuilder = new ChartFuncs(),
        charts = data,
        chartItem,
        chartType,
        chartDivId;

    for(var i=0; i<charts.length; i++) {

        chartItem = charts[i];

        chartDivId = chartItem.name+'_chart';

        // must exist on every chart dataset
        chartType = chartItem['type'];

        chartBuilder[chartType](chartDivId, chartItem.chartData);
    }
}


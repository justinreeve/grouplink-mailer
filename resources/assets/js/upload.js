$(document).ready(
    function() {

        ///////////// Upload/Download /////////////

        $('#upload-form').on('change', function(e){

            $('#errors').hide().html('');
            $('#file-preloader').show();

            $('#upload-form').ajaxForm({
                url : '/upload-leads',
                dataType : 'json',
                success : function(response) {

                    $('#file-preloader').hide();

                    if(response.success) {
                        window.location.reload();
                    } else {
                        console.warning('unexpected result: %o',response);
                    }

                },
                error: function(xhr) {

                    console.log('error: %o',xhr);
                    $('#file-preloader').hide();
                    var response = JSON.parse(xhr.responseText);

                    var messages = '<p><strong>We encountered the following errors while trying to process your upload :</strong></p>';
                    for(var i=0; i<response.errors.length; i++) {
                        messages += '<p>'+response.errors[i]+'</p>';
                    }

                    $('#errors').show().html(messages);

                }
            }).submit();

        });
    }
);


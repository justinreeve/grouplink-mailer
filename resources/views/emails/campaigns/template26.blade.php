<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <style type="text/css">
        @media only screen and (max-width:480px){
            .body{width:100%!important; min-width:100%!important;font-size: 16px;margin:0 !important;}
            img{max-width:100%}
            .sep{display:none;}
            .header table{float:none !important; width:100% !important}
            .footer p{ line-height:24px !important; font-size:20px !important}
        }
    </style>
</head>

<body style=" -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0; color:#09323e; background: #f5f5f5; font-family:Arial, Helvetica, sans-serif; font-size:16px; line-height:1.4; width:100%">

<!-- Preview text -->
<div style="display:none;font-size:1px;color:#333333;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;">
    Ever get interruptions like these during your high priority IT tasks? See if this Jedi-worthy helpdesk solution fits for your tech team
</div>
<!--End preview text-->

<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
    <tr>
        <td valign="top" align="center">
            <table class="body" width="550" cellpadding="0" cellspacing="0" border="0" style="font-family:Arial, Helvetica, sans-serif; width:550px;margin:10px; padding:0px; line-height: 1.4 !important; ">

                <!---***************** Start Content*****************-->
                <tr align="left">
                    <td style="padding-left:10px; padding-right:10px; padding-bottom: 10px; background-color:#ffffff">
                        <table>
                            <tr>
                                <td style="font-family:Arial, Helvetica, sans-serif;">
                                    <p style="margin-bottom:1em;margin-top:1em">
                                        {{$firstName}},
                                    </p>
                                    <p style="margin-bottom:1em;">Ever get interruptions like these during your high priority IT tasks?</p>
                                    <div>
                                        <ol>
                                            <li>My password doesn’t work!</li>
                                            <li>Should I be able to warm my coffee on my laptop?</li>
                                            <li>My screen doesn’t work when I touch it!</li>
                                        </ol>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family:Arial, Helvetica, sans-serif;">
                                    Use this video (1½ minute long) to see if this Jedi-worthy helpdesk solution fits for your tech team: <a style="border:none !important;color:#B3242C; font-size:.9em;" href="http://grouplink.com/ehd-intro-video/" target="_blank">http://grouplink.com/ehd-intro-video/</a>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family:Arial, Helvetica, sans-serif;">
                                    <p style="margin-top:1em;">That's it,</p>
                                    <p>
                                        <strong>Joe Nemrow</strong> <br> <br>

                                        IT K-12 Support<br>
                                        everything HelpDesk by GroupLink<br>
                                        <span style="font-style: italic">"Serving IT professionals since the Battle of Yavin"</span><br>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!---***************** End Content*****************-->
                <tr>
                    <td style="font-family:Arial, Helvetica, sans-serif; padding-left:10px; padding-top:5px;"><p style="font-size:12px">If you no longer wish to receive information on this topic, please go <a href="http://skycentral.com/omit" target="_blank" style="border:none !important;color:#09323e">here</a>. </p></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>

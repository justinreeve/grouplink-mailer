{{$firstName}},

Ever get interruptions like these during your high priority IT tasks?

- My internets isn’t working!
- Should I be able to warm my coffee on my laptop?
- What does this doohickey do?

Use this video link (1½ minute long) to see if this Jedi-worthy helpdesk solution fits for your tech team:

http://grouplink.com/ehd-intro-video

That's it,

Joe Nemrow

IT K-12 Support
everything HelpDesk by GroupLink
“Serving IT professionals since the Battle of Yavin”





If you wish to be omitted from info like this visit http://www.grouplink.com/omit

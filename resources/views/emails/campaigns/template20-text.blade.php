{{$firstName}},

How do you take advantage of these top 3 building savings with K-12 facilities?

Avoid the following:

1.  Lawsuits from injuries, facility rentals, etc.
2.  Abuse related repairs and maintenance
3.  Payroll overruns

Check out this short video on saving best-practices from the K-12 focus group :

http://skycentral.com/k12/focus-group 

Joe Nemrow 
 Customer Care  SkyCentral K-12 Reservation Engine
Serving K-12 professionals for over 20 years

Learn more about cloud tools to help K-12 professionals : http://skycentral.com/k12/re-intro-videos




  If you no longer wish to receive information on this topic, please go to http://skycentral.com/omit

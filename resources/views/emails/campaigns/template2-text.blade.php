{{$firstName}},

Your tech support team suffers through hundreds of interruptions while working on high-priority tasks.
Click this link to watch a 90-second video to see if this solution fits your organization:

http://grouplink.com/ehd-intro-video

That’s it.

Joe Nemrow
jnemrow@grouplink.net
IT K-12 Support
GroupLink Corporation
“Serving IT professionals since the Battle of Yavin”



Don't want email from Grouplink? Click here to unsubscribe: http://glmailer.cornercanyonconsulting.com/unsubscribe/{{$mailHash}}


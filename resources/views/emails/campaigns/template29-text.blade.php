{{$firstName}},

Your position at the district gives you the best insights as to who needs to hear of the latest solutions.

Please forward this to the person(s) at your district who would be the first to take action.

Top 3 building savings with K-12 facilities, by avoiding the following:

1.  Lawsuits from injuries, facility rentals, etc.
2.  Abuse related repairs and maintenance
3.  Payroll overruns

Request your no cost copy of the facility savings best-practices report from the K-12 focus group :

http://skycentral.com/k12/focus-group

Joe Nemrow 
 Customer Care  SkyCentral K-12 Reservation Engine
Serving K-12 professionals for over 20 years

Learn more about cloud tools to help K-12 professionals : http://skycentral.com/k12/re-intro-videos




  If you no longer wish to receive information on this topic, please go to http://skycentral.com/omit

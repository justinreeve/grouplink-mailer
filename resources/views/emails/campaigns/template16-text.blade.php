{{$firstName}},

How do you avoid these top 3 budget red flags from K-12 facilities?

1.  Lawsuits from injuries, facility rentals, etc.
2.  Abuse related repairs and maintenance
3.  Payroll overruns

Check out this short video on budgeting best-practices from the K-12 focus group :

http://skycentral.com/k12/focus-group 

Joe Nemrow 
 Customer Care  SkyCentral K-12 Reservation Engine
Serving K-12 professionals for over 20 years

Learn more about tools to help you avoid these red flags : http://skycentral.com/k12/re-intro-videos




  If you no longer wish to receive information on this topic, please go to http://skycentral.com/omit

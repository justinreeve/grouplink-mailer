@extends('layouts.website-inner')

@section('title')
    <title>Globio - Worldwide Travel Payments</title>
@stop

@section('header')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.1/css/selectize.bootstrap3.min.css">
@stop

@section('content')
    <div class="container">
        <div style="margin: 50px 0 50px">
            <p>Total Emails: <span><strong>{{$total}}</strong></span></p>
        </div>
        <div id="errors" class="alert alert-danger" role="alert" style="display: none"></div>
        <div>
            <form id="upload-form" action="/upload-leads" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input id="leads" name="leads" type="file"
                       class="filestyle" data-buttonText=" Upload Leads" data-badge="false" data-input="false"
                       accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,
               application/vnd.ms-excel,
               application/vnd.oasis.opendocument.spreadsheet,
               application/x-gnumeric,
               text/csv">
            </form>
            <img id="file-preloader" src="/img/preloader.gif" style="display:none">
        </div>

    </div>
@stop

@section('footer')
    <script src="//cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.0/js/standalone/selectize.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js"></script>
    <script src="/js/bootstrap-filestyle.min.js"></script>
    <script src="/js/upload.js"></script>
@stop

@extends('layouts.website-inner')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2 auth-panel">
			<div class="panel panel-default">
				<div class="panel-heading">Sign Up</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="/auth/register-staff">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="fname" value="{{ old('fname') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Your E-Mail Address</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Your Company Name</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="subdomain" value="{{ old('subdomain') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Your Company Website (Optional)</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="subdomain" value="{{ old('subdomain') }}">
							</div>
						</div>

						<div class="form-group">
							<label for="type">Company Type</label>
							@if($errors->has('type'))
								<p class="text-danger">{{$errors->first('type')}}</p>
							@endif
							<select id="type" class="form-control" name="type">
								@foreach($types as $value => $label)
									<?php
									$selectedType = old('type');
									$selectedMarker = $value == $selectedType ? 'selected': '';
									?>
									<option value="{{$value}}" {{$selectedMarker}}>{{$label}}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group">
							<label for="country">Copmany Country</label>
							@if($errors->has('country'))
								<p class="text-danger">{{$errors->first('country')}}</p>
							@endif
							<select id="country" class="form-control" name="country">
								@foreach($countries as $value => $name)
									<?php
									$selectedCountry = old('country');
									$selectedMarker = $value == $selectedCountry ? 'selected': '';
									?>
									<option value="{{$value}}" {{$selectedMarker}}>{{$name}}</option>
								@endforeach
							</select>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Your Globio Site</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="subdomain" value="{{ old('subdomain') }}">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="remember"> Remember Me
									</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
								Sign Up
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

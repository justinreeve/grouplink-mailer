<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    @yield('title')
    @yield('meta')
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Styles -->
    <!-- link rel="stylesheet" href="https://cdn.jsdelivr.net/fontawesome/4.4.0/css/font-awesome.min.css" -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">

    <link href="/css/web-styles.css" rel="stylesheet">
    <!-- Font -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,700' rel='stylesheet' type='text/css'>

    <!--[if lt IE 9]>
        <!-- script src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <!-- script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="/js/cookie.js"></script>
    @yield('header')
</head>

<body>

<!-- HEADER -->

<a href="#content" class="sr-only sr-only-focusable">Skip to main content</a>

<nav class="header">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Grouplink</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
            </ul>
          </div><!--/.navbar-collapse -->
    </div>
</nav>

<!-- CONTENT -->

<div id="content" class="content-inner">
    @yield('content')
</div>

<footer class="footer container-fluid footer-fixed">
    <div class="clearfix">
        <p class="pull-left">&copy; <a href="/">Grouplink</a> 2016</p>
    </div>
</footer>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.4/js/bootstrap.min.js"></script>

@yield('footer')
</body>
</html>

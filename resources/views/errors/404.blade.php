@extends('layouts.website-inner')

@section('title')
    <title>Globio</title>
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-9 col-md-offset-2 col-sm-offset-1 text-center">
                <h2 class="message">Oops... no page here. </h2><h3>Try starting from the <a href="/">beginning</a>. </h3>
            </div>
        </div>
    </div>
@stop
